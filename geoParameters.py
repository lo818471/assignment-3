#Stores all parameters for calculating geostrophic wind
#Contains pressure, exact value, geowind and dpdyExactly functions which we
#can use in other modules

import numpy as np

pa = 1e5        # the mean pressure
pb = 200.       # the magnitude of the pressure variations
f = 1e-4        # the coriolis parameter
rho = 1.        # density
L = 2.4e6       # length scale of the pressure variations (k)
ymin = 0        # minimum space dimension
ymax = 1e6      # maximum space dimension (k)


def pressure(y):
    "Evaluates the pressure at a given y location"
    return pa + pb*np.cos(y*np.pi/L)


def uExactly(y):
    "Evaluates the analytic geostrophic wind at given y location"
    return pb*np.pi*np.sin((y*np.pi)/L)/(rho*f*L)


def geoWind(dpdy):
    "Evaluates the geostrophic wind as a function of pressure gradient"
    return -dpdy/(rho*f)


def dpdyExactly(y):
    "Evaluates the analytic dpdy at given y location"
    return -pb*np.pi*np.sin((y*np.pi)/L)/L


