# MTMW12 Assignment 3. Katie MacKenzie
# Python3 code to numerically differentiate the pressure in order to
# calculate the geostrophic wind relation using 2-point differencing,
# compare with the analytic solution and plot

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *

def geostrophicWind():
    "Calculates the pressure gradient using the gradient_2point function."
    "Creates an array of the analytical"
    "solution, and plots the analytical solution against the numerical"
    "solution. Calculates and plots the errors."
    
    import geoParameters as gp
    
    # Resolution
    N = 10                                 # the number of intervals
    dy = (gp.ymax - gp.ymin)/N             # length of the spacing
    y = np.linspace(gp.ymin, gp.ymax, N+1) # array of spaces
    
    # Array of geostrophic wind calculated using the analytic gradient.
    uExact = gp.uExactly(y)
    
    # The pressure at the y evenly spaced points
    p = gp.pressure(y) 
    
    # The pressure gradient and wind using two point differences
    dpdy = gradient_2point(p, dy)
    u_2point = gp.geoWind(dpdy)
    
    # Graph to compare the numerical and analytic solutions
    # plot using large fonts
    font = {'size':14}
    plt.rc('font', **font)
    
    # Plot the numerical and analytical wind at y points
    plt.figure(0)
    plt.plot(y/1000, uExact, 'k-', label='Exact')
    plt.plot(y/1000, u_2point, '*k-', label='Two-point differences', \
             ms=12, markeredgewidth=1.5, markerfacecolor='none')
    plt.title('Analytic and numerical gradient 2-point solutions')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('plots/geoWindCent.pdf')

    # Create a new plot of the error between the numerical and analytical wind
    plt.figure(1)
    plt.plot(y/1000, uExact - u_2point)
    plt.title('Error in analytic and numerical solutions')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.savefig('plots/geoWindError.pdf')
    plt.show()
    

if __name__ == "__main__":
    geostrophicWind()    
    
    
