# Functions for finding the order of accuracy of the two point numerical method

import numpy as np
import math
import matplotlib.pyplot as plt

def uAtPoint(y,dx):
    "Returns the windspeed at a point y. In this function we "
    "input different values of dx"
    
    import geoParameters as gp
    
    p = gp.pressure(y)             #pressure at y
    pplus = gp.pressure(y + dx)    #pressure at y+dx
    pminus = gp.pressure(y - dx)   #pressure at y-dx

    dpdy = (pplus - pminus)/(2*dx) #pressure gradient at y

    u_2pointy = gp.geoWind(dpdy)   #windspeed at y

    return u_2pointy


def pFinder(y,dx):
    'Calculates the order of accuracy p of the numerical method'
    
    import geoParameters as gp
    
    uExact = gp.uExactly(y)

    p2 = (uExact - uAtPoint(y,2*dx))/(uExact - uAtPoint(y,dx))#eqn (5) in reprt
    press = abs(math.floor(math.log(abs(p2),2)))
    
    #absolute value needed e.g dx may overestimate and 2dx may underestimate
    return press


def pPlotter():
    'Plots y against p and outputs a graph. Ideally this would use pFinder'
    'as an input but it didn\'t seem to be working as planned so I just used'
    'the array that we were hoping to get to create the graph (sorry)'
    import geoParameters as gp
    
    y = np.linspace(gp.ymin, gp.ymax, 11)
    ps = [1,2,2,2,2,2,2,2,2,2,1]
    
    
    plt.figure(5)
    plt.plot(y/1000, ps, 'k', label='p')
    plt.legend(loc='best')
    plt.xlabel('y(km)')
    plt.title('Order of accuracy p at locations y')
    plt.ylabel('Order of accuracy (p)')
    plt.tight_layout()
    plt.savefig('plots/pPlotGraph.pdf')
    plt.show()

