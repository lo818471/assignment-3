#This is a function using the richardson method to calculate a more accurate
#numerical method

import numpy as np
from differentiate import *
import matplotlib.pyplot as plt
import orderFunctions as oFn

def richardson():
    'Uses the richardson technique to take a numerical method of known order'
    'of accuracy p and creates a new numerical method of unknown but greater'
    'order of accuracy q'

    import geoParameters as gp
    N = 10                                 # number of intervals 
    dy = (gp.ymax - gp.ymin)/N             # length of the spacing 
    y = np.linspace(gp.ymin, gp.ymax, N+1) # array of y
    p = gp.pressure(y)                     # array of pressure
    
    # The pressure gradient and wind using two point differences
    dpdy = gradient_2point(p, dy)
    u_2point = gp.geoWind(dpdy)

    # Pressure gradient and wind at 2*dy
    dpdy2 = gradient_2point(p, 2*dy)
    u_2point2 = gp.geoWind(dpdy2)

    # Calculating the more accurate discretisation scheme
    moreAccurate = np.zeros_like(dpdy)

    moreAccurate[0] = 2*u_2point[0] - u_2point2[0]
    moreAccurate[-1] = 2*u_2point[-1] - u_2point2[-1]

    for i in range(1, len(moreAccurate)-1):
        moreAccurate[i] = (4*u_2point[i] - u_2point2[i])/3

    uExact = gp.uExactly(y)
    
    # Plot the numerical and analytical wind at y points
    plt.figure(3)
    plt.plot(y/1000, uExact, 'k-', label='Exact')
    plt.plot(y/1000, u_2point, '*k-', label='Richardson method', \
             ms=12, markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.title('The analytic solution and Richardson solution at y')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('plots/geoWindRich.pdf')

    # Create a new plot of the error between the numerical and analytical wind
    plt.figure(4)
    plt.plot(y/1000, uExact - moreAccurate)
    plt.title('The error in the Richardson solution')
    plt.xlabel('y (km)')
    plt.ylabel('Error (m/s)')
    plt.savefig('plots/geoWindRichError.pdf')


richardson()
