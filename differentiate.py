# Function for calculating the gradient using a second order finite difference
# method

import numpy as np

def gradient_2point(f,dx):
    "Calculates the gradient of array f where points are a distance dx apart"
    "using the 2-point differentiation numerical method. Uses centered"
    "differences for the middle values and 2-point differences for the"
    "endpoints. Outputs an array of gradients."
    
    # Initialising the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    
    # Two point differences at the end points
    dfdx[0] = (f[1] - f[0])/ dx
    dfdx[-1] = (f[len(f)-1] - f[len(f) - 2])/ dx
    
    # Centred differences for the mid-points
    for i in range(1,len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1])/ (2*dx)
    return dfdx
